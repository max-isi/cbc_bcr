import os
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
matplotlib.style.use('classic')

mplparams = {
    'text.usetex': True,  # use LaTeX for all text
    'axes.linewidth': 1,  # set axes linewidths to 0.5
    'axes.grid': False,  # add a grid
    'axes.labelweight': 'normal',
    'font.family': 'serif',
    'font.size': 24,
    'font.serif': 'Computer Modern Roman'
}
matplotlib.rcParams.update(mplparams)

def plot2d(x, y, mkey='', method='hexbin', cm='gray_r', figdir='',
           margin=0.05, alpha=1, suffix='', xlabel='', ylabel='', ext='png'):
    method = method.lower()
    fig, ax = plt.subplots(1)
    cmap = plt.get_cmap(cm)
    # plot with indicated method
    if method != 'seaborn':
        if method == 'scatter':
            im = ax.scatter(x, y, marker='s', lw=0, s=60, alpha=alpha,
                            cmap=cmap)
        elif method == 'hexbin':
            im = ax.hexbin(x, y, cmap=cmap, gridsize=35, alpha=alpha)
        else:
            raise ValueError("unsupported plot method %r" % method)
        range_x = x.max() - x.min()
        range_y = y.max() - y.min()
        ax.set_xlim(x.min() - margin*range_x, x.max() + margin*range_x)
        ax.set_ylim(y.min() - margin*range_y, y.max() + margin*range_y)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        cb = plt.colorbar(im, ax=ax)
    else:
        import seaborn as sns
        sns.set(font_scale=1.5)
        sns.axes_style("white")
        sns.set_style("ticks")
        g = (sns.jointplot(x=x, y=y, kind="hex", color="k", stat_func=None)
                .set_axis_labels(xlabel, ylabel, fontsize=20))
        cax = g.fig.add_axes([0.675, .15, .015, .3])
        cb = plt.colorbar(cax=cax)
        cb.ax.tick_params(labelsize=12)
    cb.set_label(r'${\rm Count}$')
    figpath = os.path.join(figdir, 'hist2d_%s_bci_s-%s%s.%s'
                           % (method, mkey, suffix, ext))
    plt.savefig(figpath, bbox_inches='tight')
    print "Figure saved: %s" % figpath
    plt.close(fig)
    if method == 'seaborn':
        sns.reset_orig()

