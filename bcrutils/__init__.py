import matplotlib
import matplotlib.style
matplotlib.use('Agg')
try:
    matplotlib.style.use('classic')
except:
    pass

mplparams = {
    'text.usetex': True,  # use LaTeX for all text
    'axes.linewidth': 1,  # set axes linewidths to 0.5
    'axes.grid': True,  # add a grid
    'axes.labelweight': 'normal',
    'font.family': 'serif',
    'font.size': 18,
    'font.serif': 'Computer Modern Roman'
}
matplotlib.rcParams.update(mplparams)

new_colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
              '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
              '#bcbd22', '#17becf']

from cycler import cycler
matplotlib.rcParams['axes.prop_cycle'] = cycler(color=new_colors)

from .bcrutils import *
