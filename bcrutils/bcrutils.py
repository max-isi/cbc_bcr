#! /usr/bin/env python

import os, sys
import numpy as np
import pandas as pd
import h5py
import json
from copy import copy
from glob import glob
from warnings import warn
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
#matplotlib.style.use('classic')
from scipy.misc import logsumexp
import progressbar

# mplparams = {
#     'text.usetex': True,  # use LaTeX for all text
#     'axes.linewidth': 1,  # set axes linewidths to 0.5
#     'axes.grid': True,    # add a grid
#     'axes.labelweight': 'normal',
#     'font.family': 'serif',
#     'font.size': 18,
#     'font.serif': 'Computer Modern Roman'
# }
# matplotlib.rcParams.update(mplparams)

DEFAULT_BCR_WEIGHTS = (1, 1)
ALL_SEGLENS_STR = ['4', '8', '16', '32', '64', '128']
ALL_SEGLENS = [int(sl) for sl in ALL_SEGLENS_STR]

# Chirp-mass bounds are given in Table I of arXiv:1604.08253. The upper limit
# for the 4s basis was doubled around Jan 2017; all other bins remain the same.
ROQ_MASS_BINS = {
    4: (12.299703, 44.749004),
    8: (7.9, 14.8),
    16: (5.2, 9.5),
    32: (3.4, 6.2),
    64: (2.2, 4.2),
    128: (1.4, 2.6),
}

# Default priors for LALInference runs using ROQ
ROQ_DEFAULT_PRIORS = {
    'qlim': (0.125, 8),
    'alim': (0, 0.89),
    'dlim': (1, 2e3)
}


# ############################################################################
# BCR FUNCTIONS

def compute_lnbcr(lnbsn, *single_lnbs, **kwargs):
    """ Compute a coherent vs incoherent (or noise) log Bayes factor.

    Can tune hypothesis prior using following weight ratios:
        alpha = P(H_S) / P(H_I)
        beta  = P(H_G | H_I) = 1 - P(H_N | H_I)
    As usual, this will assume `P(H_N | H_I) +  P(H_G | H_I) = 1`.
    See Eq. (1) in BCR paper for reference.

    Arguments
    ---------
    lnbsn : float
        coherent log Bayes factor
    single_lnbs : float(s)
        as many single-IFO signal vs noise lnBs as detectors (one per argument)
        there must be one argument per IFO, and there should at least be two.
    mode : str
        determine how to compute BCR ('bcr', 'bci', or 'bcr2') [def: 'bcr'].
    weights : tuple
        weight ratios for 'alpha = signal vs instrument' and 
        'beta = glitch vs Gaussian noise' hypotheses (alpha, beta) [def: (1,1)]

    Returns
    -------
    lnbsr : float
        coherent vs incoherent (or noise) log bayes factor.
    """
    # parse kwargs
    if len(single_lnbs) < 2:
        raise ValueError("need at least 2 single-IFO lnBSNs (%i)" 
                         % len(single_lnbs))
    weights_def = DEFAULT_BCR_WEIGHTS  # (S/I, G/N)
    weights = kwargs.pop('weights', weights_def)
    if len(weights) != 2:
        raise ValueError("need two weights (s/i, g), not %r" % weights)
    mode_df = 'bcr' if weights==weights_def else 'weights'
    mode = kwargs.pop('mode', mode_df).lower()
    if kwargs:
        print "WARNING: unrecognized options:\n%r" % kwargs
    # process
    output = copy(lnbsn)
    if mode == 'bcr':
        # The two expressions should be equivalent, but the bottom one is
        # possibly marginally more efficient: 
        # output = np.log(np.exp(lnbsn)/((np.exp(lnbh1) + 1)*(np.exp(lnbl1)+1)))
        # output = lnbsn - np.logaddexp(lnbh1, 0) - np.logaddexp(lnbl1, 0)
        for lnb in single_lnbs:
            output += - np.logaddexp(lnb, 0)
    elif mode == 'weights':
        # BCR as above, but with relative weights for the different hypotheses
        #     wg = P(H_G | H_I) = beta
        #     wn = P(H_N | H_I) = 1 - beta
        alpha, beta = weights
        wg, wn = beta, 1 - beta
        output += np.log(alpha)
        for lnb in single_lnbs:
            output += - np.logaddexp(lnb + np.log(wg), np.log(wn))
    elif mode == 'bci':
        output += - np.sum(single_lnbs)
    # elif mode == 'bcr2':
    #     bs = [np.exp(lnb) for lnb in single_lnbs]
    #     output = logsumexp([lnbsn] + list(single_lnbs))
    #     if any([b > 100 for b in bs]):
    #         #output = logsumexp([lnbsn, lnbh1, lnbl1]) - lnbh1 - lnbl1
    #         output += - np.sum(single_lnbs)
    #     else:
    #         output += - np.log(np.prod(bs) + 1)
    else:
        raise ValueError("invalid BCR mode %r" % mode)
    return output


def log_exp(x):
    return x/np.log(10.)


def pvalue(samples, value):
    """Computes p-value computed from simple ordering of the samples.
    (Right handed tail).
    """
    sorted_samples = np.sort(samples)
    value_placement = float(sorted_samples.searchsorted(value))
    pvalue = 1. - value_placement/len(samples)
    if pvalue <= 0:
        pvalue = 1./len(samples)
    return pvalue


def print_lnbcr_stats(series):
    print "Top logBCRs:"
    for i, v in series.sort_values(ascending=False).head().iteritems():
        print "%.2f\t%.2f" % (i, log_exp(v))
    print "Bottom logBCRs:"
    for i, v in series.sort_values(ascending=True).head().iteritems():
        print "%.2f\t%.2f" % (i, log_exp(v))
    print "Percent greater than zero: %.2f" % pvalue(series.values, 0)
    print "Percent greater than one: %.2f" % pvalue(series.values, 1)
    print "Percent greater than two: %.2f" % pvalue(series.values, 2)


# ############################################################################
# PARAMETER FUNCTIONS

def chirpmass(m1, m2):
    return (m1 * m2)**(3./5.) / (m1 + m2)**(1./5.)


def mc_q(m1, m2):
    return chirpmass(m1, m2), m2/m1 


def m1_m2(mc, q):
    m1 = mc * (1 + q)**(1./5) / q**(3./5)
    m2 = m1 * q
    return m1, m2


def chieff_from_m1m2(m1, m2, a1z, a2z):
    # Definition from https://arxiv.org/pdf/1706.01812.pdf
    return (m1*a1z + m2*a2z) / (m1 + m2)


def chieff_from_mcq(mc, q, a1z, a2z):
    # Definition from https://arxiv.org/pdf/1706.01812.pdf
    m1, m2 = m1_m2(mc, q)
    return (m1*a1z + m2*a2z) / (m1 + m2)


def mass_bin(m1=None, m2=None, mc=None):
    """Categorize trigger into LALInference ROQ bins [4s, 8s, ..., 128s]
    according to chirp mass.

    Arguments
    ---------
    m2: float
        component mass in solar masses
    m1: float
        component mass in solar masses
    
    Returns
    -------
    seglens: list
        corresponding segment lengths [4, 8, 16, ..., 128]
    """
    if not mc:
       mc  = chirp_mass(m1, m2)
    seglens = []
    for seglen, bounds in ROQ_MASS_BINS.iteritems():
        if ((bounds[0] < mc) & (mc < bounds[1])):
            seglens.append(seglen)
    if not seglens:
        print "Warning: mc (%.2f) outside ROQ priors" % mc
        seglens.append(0)
    return seglens


def a_from_comps(ax, ay, az):
    return np.sqrt(ax**2 + ay**2 + az**2)


def a1a2_from_bank(bank, i):
    a1 = a_from_comps(bank['spin1x'][i], bank['spin1y'][i], bank['spin1z'][i])
    a2 = a_from_comps(bank['spin2x'][i], bank['spin2y'][i], bank['spin2z'][i])
    return a1, a2


def mcq_from_bank(bank, tempid):
    return mc_q(bank['mass1'][tempid], bank['mass2'][tempid])


def classify_signals(mc=None, q=None, a1=None, a2=None, d=None, 
                     qlim=(0.125, 8), alim=(0, 0.89), dlim=(0, 5e3)):
    """Categorize signal based on component masses. Criterion based
    on bounds for prior F in Table I of arXiv:1604.08253.
    
    Arguments
    ---------
    mc: float, array
        chirp-mass (Msun)

    m1: float, array
        first component mass (Msun)
    
    m2: float, array
        second component mass (Msun)

    a1: float, array
        first component spin magnitude (0,1)

    a2: float, array
        second component spin magnitude (0,1)

    d: float, array
        distance to source (Mpc)
        Note default is for extended runs with 5000 Mpc, LALInference default
        is 2000 Mpc.
    
    Returns
    -------
    isgood: bool
        whether trigger falls in distance, mass & spin prior.
    seglens: list
        list of seglens in which trigger falls [4, 8, ..., 128]
    """
    # mass check
    try:
        mc[0]
    except KeyError:
        mc.iloc[0]
    except:
        mc = np.array([mc])
    minseglens = np.array([min(mass_bin(mc=m)) for m in mc])
    isgood = minseglens > 0
    if q is not None and qlim is not None:
        isgood = isgood & (qlim[0] < q) & (q < qlim[1])
    if d is not None and dlim is not None:
        isgood = isgood & (dlim[0] < d) & (d < dlim[1])
    if a1 is not None and alim is not None:
        isgood = isgood & (alim[0] < a1) & (a1 < alim[1])
    if a2 is not None and alim is not None:
        isgood = isgood & (alim[0] < a2) & (a2 < alim[1])
    return isgood, minseglens


def classify_trigger(bank, tempid, qlim=(0.125, 8), alim=(0, 0.89)):
    """Categorize template from ID, based on component masses. Criterion based
    on bounds for prior F in Table I of arXiv:1604.08253.
    
    Arguments
    ---------
    tempid: int
        template_id corresponding to index in bank file.
    
    Returns
    -------
    isgood: bool
        whether trigger falls in distance, mass & spin prior.
    seglens: list
        list of seglens in which trigger falls [4, 8, ..., 128]
    """
    mc, q = mcq_from_bank(bank, tempid)
    seglens = mass_bin(mc=mc) 
    a1, a2 = a1a2_from_bank(bank, tempid)
    isgood = (len(seglens) >= 1) & (qlim[0] < q) & (q < qlim[1]) &\
             (alim[0] <= a1) & (a1 < alim[1]) & (alim[0] <= a2) & (a2 < alim[1])
    return isgood, seglens


# ############################################################################
# UTILITY FUNCTIONS

def tokenize(string, **kwargs):
    """Performs common wildcard substitutions in strings.

    Example:
        $ tokenize('my_string_(IFO).ext', ifo='H1')
        "my_string_H1.ext"

    :param string: string on which to effect substitution (str).
    :param kwargs: token = "string" pairs to substitute (str).
    """
    if string is not None:
        for wildcard, meaning in kwargs.iteritems():
            string = string.replace("(%s)" % wildcard.upper(), meaning)
    return string


def get_metadata(filename, key):
    # From /home/max.isi/local/libexec/lalapps/lalapps_coherence_test.py
    with h5py.File(filename, 'r') as hdf:
        g = hdf['lalinference']
        if(len(g.keys()))>1:
            print('Error: multiple runs %s found in input file %s'
                  % (g.keys(), filename))
            sys.exit(1)
        # Descend into run group
        g = g[g.keys()[0]]
        return g.attrs[key]


def progress_bar(maxval):
    """Returns a standard progress bar."""
    return progressbar.ProgressBar(maxval=maxval,
                                   widgets=[progressbar.Bar('=', '[', ']'),
                                   ' ', progressbar.Percentage()])


def get_bsn(path):
    # detect whether HDF5 or old ASCII LALInference output
    ext = os.path.splitext(path)[1]
    if 'hdf' in ext:
        lnbsn = get_metadata(path, 'log_bayes_factor')
    elif os.path.isfile(path + '_B.txt'):
        lnbsn = np.loadtxt(path + '_B.txt')[0]
    else:
        raise IOError("cannot read BSN.")
    return lnbsn


def read_bsns(path, ifos=['h1', 'l1'], single_paths=None):
    ''' Read lnBSNs from file.
    '''
    ifo_str = str(IFOList(ifos))
    path = tokenize(path, ifo=ifo_str)
    lnbsn = get_bsn(path)
    if single_paths is None:
        print "WARNING: guessing single paths from coherent path."
        paths = {i: path.replace(ifo_str, i.upper()) for i in ifos}
    else:
        paths = dict(zip(ifos, single_paths))
    lnbsns = {ifo: get_bsn(path) for ifo, path in paths.iteritems()}
    row = {'lnbsn_%s' % k: v for k,v in lnbsns.items()}
    row['lnbsn'] = lnbsn
    return row


def read_bcr(*args, **kwargs):
    weights = kwargs.pop('weights', DEFAULT_BCR_WEIGHTS)
    # load BSNs
    lnbsns = read_bsns(*args, **kwargs)
    lnbsn_coh = lnbsns.pop('lnbsn')
    # compute BCR
    lnbcr = compute_lnbcr(lnbsn_coh, *lnbsns.values(), weights=weights)
    return lnbcr, lnbsn_coh, lnbsns


def read_index(path, key):
    '''Read PyCBC file index.'''
    with open(path, 'r') as f:
        index = json.load(f)
    return index[key]


def read_stats(path):
    return pd.read_csv(path, sep='\t', index_col=0)


# ############################################################################
# CLASSES

class IFOList(list):
    def __eq__(self, other):
        return set(self) == set(other)

    def __str__(self):
        ifo_str = ''
        for ifo in self:
            ifo_str += ifo.upper()
        return ifo_str
    
    def lower(self):
        return IFOList([ifo.lower() for ifo in self])

    def upper(self):
        return IFOList([ifo.upper() for ifo in self])


# ############################################################################
# LEGACY FUNCTIONS

def collect_lalinf(*args, **kwargs):
    """ DEPRECATED! Use `trigger.TriggerDataFrame.from_lalinference`
    """
    print "WARN: deprecated! Use `trigger.TriggerDataFrame.from_lalinference`"
    from .triggers import TriggerDataFrame
    return TriggerDataFrame.from_lalinference(*args, **kwargs)


def collect_lalinf_with_cat(*args, **kwargs):
    """ DEPRECATED! Use `trigger.TriggerDataFrame.from_lalinference_with_cat`
    """
    print "WARN: deprecated! Use `TriggerDataFrame.from_lalinference_with_cat`"
    from .triggers import TriggerDataFrame
    return TriggerDataFrame(*args, **kwargs)


def apply_vetos(df, *args, **kwargs):
    """ DEPRECATED! Use `trigger.TriggerDataFrame.veto`
    """
    warn("`apply_vetos` deprecated, use `TriggerDataFrame.veto`")
    from .triggers import TriggerDataFrame
    return TriggerDataFrame(df).veto(*args, **kwargs)


def read_catalogue(path, preserve_index=False):
    warn("`read_catalogue` deprecated, use `TriggerDataFrame.read_catalogue`")
    from .triggers import TriggerDataFrame
    return TriggerDataFrame.read_catalogue(path, preserve_index)


# NOTE: is this function needed?
def prune_df(df):
    """ Apply common checks to trigger file: SNR consistency and injection 
    priors.

    Arguments
    ---------
    df : pandas.DataFrame
        original trigger set

    Returns
    -------
    df: pandas.DataFrame
        pruned trigger set
    """
    warn("`prune_df` is deprecated, use `apply_vetos` instead.")
    isgood = df['consistent_cohinc_snrs_h1'].apply(bool) &\
             df['consistent_cohinc_snrs_l1'].apply(bool)
    if 'isgood_injection' in df:
        isgood = isgood & df['isgood_injection']
    return df[isgood]


def df_from_hdf5(path):
    return pd.read_hdf(path, 'table', mode='r')


def df_to_hdf5(df, path):
    pd.DataFrame(df).to_hdf(path, 'table', mode='w')


def compute_snr_ratio(df):
    warn("`compute_snr_ratio` deprecated, use `TriggerDataFrame method`")
    # get snr keys like 'snr_coh_h1_maP'
    keys = df.keys()[df.keys().str.match('snr_coh_[a-z][0-9]_maP')]
    if len(keys) == 0:
        raise ValueError("no keys matching 'snr_coh_[a-z][0-9]_maP'")
    else:
        # get min/max and compute ratio
        minsnr = df[keys].min(axis=1) 
        maxsnr = df[keys].max(axis=1) 
        snrratio = minsnr/maxsnr 
    return snrratio

