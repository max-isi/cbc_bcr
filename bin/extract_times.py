import pandas as pd

def read_catalogue(path):
    trigcat = pd.read_hdf(path, 'table', mode='r')
    trigcat.set_index('trigtime', inplace=True)
    return trigcat

path = '/home/max.isi/projects/cbc_bci/runs/o1_bin2_bayesw-psd/triggers/trigger_catalogue.hdf5'

trigcat = read_catalogue(path)
trigtimes = trigcat.index.values

t1 = trigtimes - trigcat['dt1']
t2 = trigtimes - trigcat['dt2']
time_df = pd.DataFrame({'t1': t1, 't2': t2})

fpath = 'chosen_times_H1L1.dat'
time_df.to_csv(fpath, sep='\t', header=False, index=False, float_format='%.7f')
print "Times saved: %r" % fpath
