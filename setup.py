#!/usr/bin/env python

from distutils.core import setup

setup(name='bcrutils',
      version='0.0',
      description='BCR',
      author='Maximiliano Isi',
      author_email='max.isi@ligo.org',
      url='https://',
      packages=['bcrutils'],
     )
